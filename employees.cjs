const fs = require('node:fs')

function employees() {
    try {
        fs.readFile('../data.json', 'utf-8', (err, data) => {
            if (err) {
                console.error(err.message)
            }
            else {
                //console.log(data)
                console.log("reading file successful")
                const convertData = JSON.parse(data)
                const identities = [2, 13, 23]
                const employeeData = convertData['employees'].reduce((information, current) => {
                    if (identities.includes(current.id)) {
                        information.push(current)
                    }
                    return information
                }, [])
                //console.log(employeeData)
                fs.writeFile('../output/output1.json', JSON.stringify(employeeData), (err1) => {
                    if (err) {
                        console.error(err1)
                    }
                    else {
                        console.log("successfully retrieved employeed ids data")
                        const grouping = convertData['employees'].reduce((names, current) => {
                            if (names[current.company]) {
                                names[current.company].push(current)
                            }
                            else {
                                names[current.company] = []
                                names[current.company].push(current)
                            }
                            return names
                        }, {})
                        //console.log(grouping)
                        fs.writeFile('../output/output2.json', JSON.stringify(grouping), (err2) => {
                            if (err2) {
                                console.error(err2)
                            }
                            else {
                                console.log("grouping data successful")
                                const employeesInCompany = grouping['Powerpuff Brigade']
                                //console.log(employeesInCompany)
                                fs.writeFile('../output/output3.json', JSON.stringify(employeesInCompany), (err3) => {
                                    if (err3) {
                                        console.error(err3)
                                    }
                                    else {
                                        console.log("successfully retrieved powerpuff brigade company details")
                                        const newData = JSON.parse(JSON.stringify(convertData))
                                        newData['employees'].splice(newData['employees'].findIndex(current => current.id == 2), 1)
                                        fs.writeFile('../output/output4.json', JSON.stringify(newData), (err4) => {
                                            if (err4) {
                                                console.error(err4)
                                            }
                                            else {
                                                console.log("deleted id 2 details from data")
                                                const arrange = Object.entries(grouping).sort()
                                                const result = arrange.reduce((accumulator, current) => {
                                                    const sorting = current[1].sort((value1, value2) => value1.id - value2.id)
                                                    accumulator[current[0]] = sorting
                                                    return accumulator
                                                }, {})
                                                //console.log(result)
                                                fs.writeFile('../output/output5.json', JSON.stringify(result), (err5) => {
                                                    if (err5) {
                                                        console.error(err5)
                                                    }
                                                    else {
                                                        console.log("sorted data according to companies")
                                                        const first = convertData['employees'].findIndex(search => search.id == 92)
                                                        const second = convertData['employees'].findIndex(search1 => search1.id == 93)
                                                        const temporary = convertData['employees'][first].company
                                                        convertData['employees'][first].company = convertData['employees'][second].company
                                                        convertData['employees'][second].company = temporary
                                                        //console.log(convertData)
                                                        fs.writeFile('../output/output6.json', JSON.stringify(convertData), (err6) => {
                                                            if (err6) {
                                                                console.log(err6)
                                                            }
                                                            else {
                                                                console.log("swaping successfull")
                                                                convertData['employees'].forEach(current => {
                                                                    if (current.id % 2 == 0) {
                                                                        current['DateOfBirth'] = new Date().toLocaleDateString()
                                                                    }
                                                                })
                                                                //console.log(convertData)
                                                                fs.writeFile('../output/output7.json',JSON.stringify(convertData), (err7) => {
                                                                    if (err7) {
                                                                        console.error(err7)
                                                                    }
                                                                    else {
                                                                        console.log("date of birth added successfully")
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }
    catch (error) {
        console.error(error)
    }
}

module.exports = employees

